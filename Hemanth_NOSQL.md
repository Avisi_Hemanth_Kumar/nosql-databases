# NOSQL DATABASES

  NOSQL databases also known as (Not Only SQL) databases store data in non-relational form in the format of Document Databases, key value databases, wide column stores and graph databases.  
  
## FEATURES  

  1. The data can be stored in Non-Tabulated format.
  2. This is a Non-Relational database.
  3. No relation between the data in the database.  
  4. Both Vertical and Horizontal scaling is possible.
  5. It’s a schemaless database.
  6. There are no predefined schemas.  
  7. It uses hashing to handle the data to the database.  
  
## ADVANTAGES

  1. As there is no relationship between the data and the data stands on its own.  
  2. As there was no predefined schema it is considered as schemaless.
  3. Vertical scaling is limited to certain points but horizontal scaling is unlimited. We can scale an unlimited number of servers.  
  4. Scales easily with large amounts of data and high user loads.  
  
## DRAWBACKS

  1. Limited in Retrieving Data, Only allows in Retrieving the Data using primary key. Finding orders by ID is not a problem. But, finding the order with the amount would be very inefficient.  
  2. Eventually consistent, when we write a new item to the database and try to read it back straight away, It might not be returned. NOSQL splits the database into partitions with each partition mirrored across multiple servers. That way the server can go down without much impact. When we add a new item to the database one of the mirrors stores the item and the other mirrors can copy in the background. If we try to access the data it will not retrieve.

## NOSQL DATABASES Considered for Investigation

  1. MongoDB
  2. CouchDB  
  3. Neo4j  
  4. Redis  
  5. Apache Cassandra
  
## MongoDB

  MongoDB is one of the popular NoSQL databases that stores data in BSON. This is binary encoded JSON which stores data in key-value pairs.
  
### Features of MongoDB

  There are no tables in MongoDB. All the data is stored in JSON format.It means key-value pairs. In JSON, we define a unique key with a value associated with it. These key-value pairs are stored in a document, which in turn stored in a collection. A collection in MongoDB can have any number of documents. Each document can have any number of key-value pairs. 

### Advantages of MongoDB

  1. MongoDB is a schema-less NoSQL database. We do not need to design the schema of the database.
  2. No complex joins are needed in MongoDB. There is no relationship among data in MongoDB.  
  3. MongoDB is easy to scale.
  4. As MongoDB uses JSON format to store data, it is very easy to store arrays and objects.  
  5. MongoDB is free to use. There is no cost for it.  
  6. Performance of MongoDB is much higher compared to any relational database.
  7. There is no need for mapping of application objects to database objects in MongoDB.  
  8. MongoDB uses internal memory for storage which enables faster access to the data.  
  
### Drawbacks of MongoDB

  1. MongoDB uses high memory for data storage.  
  2. There is a limit for document size.  
  3. There is no transaction support in MongoDB.  
  
## CouchDB

  1. It is an open-source database that uses various different formats and protocols to store, transfer, and process its data.
  2. It uses JSON to store data, JavaScript is the query language using MapReduce, and HTTP for an API.
  3. Documents are the primary unit of data in CouchDB and they also include metadata.
  4. Document fields are uniquely named and it contain values of varying types and there is no set limit to text size or element count.  
  
### Features of CouchDB

  1. NoSQL database that follows document storage where each field is uniquely named and contains values of various data types such as text, number, Boolean, lists.
  2. The main reason for the popularity of CouchDB is a map/reduce system.  
  3. CouchDB facilitates us to keep authentication open via a session cookie-like a web application.  
  4. CouchDB can replicate to devices like smartphones that have a feature to go offline and handle data sync for us when the device is back online.
  5. CouchDB guarantees eventual consistency to provide both availability and partition tolerance.  

### Advantages of CouchDB

  1. HTTP API is used for easy Communication.  
  2. It is used to store any type of data.  
  3. ReduceMap allows optimizing the combining of data.  
  4. Structure of CouchDB is very simple.
  5. Fast indexing and retrieval.  

### Drawbacks of CouchDB

  1. CouchDB takes a large space for overhead, which is a major disadvantage as compared to other databases.  
  2. Arbitrary queries are expensive.  
  3. There is a bit of extra space overhead with CouchDB compared to most alternatives.  
  4. Temporary views on huge datasets are very slow.  
  5. It doesn’t support transactions.

## Neo4j  
  
### Features of Neo4j

  1. It follows Property Graph Data Model.
  2. It supports Indexes by using Apache Lucene.  
  3. It supports UNIQUE constraints.  
  4. It contains a UI to execute CQL Commands : Neo4j Data Browser.  
  5. It supports full ACID(Atomicity, Consistency, Isolation and Durability) rules.  
  6. It uses Native graph storage with Native GPE(Graph Processing Engine).
  7. It supports exporting of query data to JSON and XLS format.
  8. It provides REST API to be accessed by any Programming Language like Java, Spring,Scala etc.  
  9. It provides Javascript to be accessed by any UI MVC Framework like Node JS.  
  10. It supports two kinds of Java API: Cypher API and Native Java API to develop Java applications.  

### Advantages of Neo4j

  1. It is very easy to represent connected data.  
  2. It is very easy and faster to retrieve/traversal/navigation of more Connected data.  
  3. It represents semi-structured data very easily.  
  4. Neo4j CQL query language commands are in humane readable format and very easy to learn.  
  5. It uses a simple and powerful data model.  
  6. It does NOT require complex Joins to retrieve connected/related data as it is very easy to retrieve it's adjacent node or relationship details without Joins or Indexes.  

### Drawbacks of Neo4j

  1. AS of Neo4j 2.1.3 latest version, it has a limitation of supporting a number of Nodes, Relationships and Properties.  
  2. It does not support Sharding.  

## Redis  

### Features of Redis

  1. Redis is an open-source (BSD licensed), in-memory data structure store, used as a database, cache, and message broker.  
  2. It supports data structures such as strings, hashes, lists, sets, sorted sets with range queries, bitmaps, hyperloglogs, geospatial indexes with radius queries and streams.  
  3. In short, Redis is a tool in the In-Memory Databases category of a tech stack.  
  4. It supports caching. Caching is the process of storing some data in Cache.  
  
### Advantages of Redis

  1. It’s super fast. Faster than any other cashing out there.  
  2. Redis has flexible data structures, it supports almost all data structures.  
  3. Redis allows storing key and value pairs as large as 512 MB.  
  4. Redis uses its own hashing mechanism called Redis Hashing.  
  5. Zero downtime or performance impact while scaling up or down.  

### Drawbacks of Redis

  1. Since Data is sharded based on the hash-slots assigned to each Master. If the Master holding some slots is down, data to be written to that slot will be lost.  
  2. Clients connecting to the Redis cluster should be aware of the cluster topology, causing overhead configuration on Clients.  
  3. Failover does not happen unless the master has at least one slave.  
  4. It requires a huge ram because it is in-memory so we are not supposed to use it on ram servers.  

## Apache Cassandra
  
### Features of Apache Cassandra

  1. Apache Cassandra is an open-source NoSQL database management system known for its high availability and scalability.  
  2. Cassandra can handle massive amounts of data and provide real-time analysis.
  3. Unlike traditional databases, NoSQL databases like Cassandra don't require schema or a logical category to store large data quantities.  
  4. Cassandra is written in Java and open-sourced in 2008. Organizations and companies like AppScale, Constant Contact, Digg, Facebook, IBM, Instagram, Spotify, Netflix, and Reddit favor it.  

### Advantages of Apache Cassandra

  1. It’s open-source.
  2. It follows peer-to-peer architecture rather than master-slave architecture, so there isn’t a single point of failure.
  3. Cassandra can be easily scaled down or up.
  4. It features data replication, so it’s fault-tolerant and has high availability.
  5. It’s a high-performance database manager that easily handles massive amounts of data.
  
### Drawbacks of Apache Cassandra

  1. Because it handles large amounts of data and many requests, transactions slow down, meaning we get latency issues.
  2. Data is modeled around queries and not structure, resulting in the same information stored multiple times.
  3. Since Cassandra stores vast amounts of data, we may experience JVM memory management issues.
  4. It offers no join or subquery support.
  5. Cassandra doesn’t support aggregates.
  6. Cassandra was optimized from the start for fast writes, reading got the short end of the stick, so it tends to be slower.  
  7. Finally, it lacks official documentation from Apache, so we need to look for it among third-party companies.

## Conclusion

  After Investigating 5 different NOSQL Databases and comparing the advantages and limitations, I came to the conclusion that MongoDB leads the race ahead. Inorder to solve performance and scalability issues for our Database we can choose MongoDB database.

## Reference Links

  1. [MongoDB Website](https://www.mongodb.com/nosql-explained)
  2. [NoSQL video explanation](https://www.youtube.com/watch?v=0buKQHokLK8&t=70s)

  3. [Redis Documentation](https://redis.io/documentation)
  4. [Apache Casendra](https://blog.knoldus.com/is-apache-cassandra-really-the-database-you-need/)
  5. [Couch DB](https://www.geeksforgeeks.org/introduction-to-apache-couchdb/)
